from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login as login_func, logout as logout_func
from .forms import SignUpForm
def index(request):
    return render(request, "landingpage.html")

def login(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login_func(request, user)
            return redirect('/')
        else:
            form = SignUpForm()
            return render(request, 'login.html', {'form': form})
    else:
        form = SignUpForm()
    return render(request, 'login.html', {'form': form})

def logout(request):
    logout_func(request)
    return redirect('/')
