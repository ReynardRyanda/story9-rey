from django.urls import path, include
from .views import *

app_name = 'login'

urlpatterns = [
    path('',index,name='index'),
    path('login/',login,name='login'),
    path('logout/',logout,name='logout'),
]