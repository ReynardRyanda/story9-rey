from django.test import TestCase
from django.urls import resolve
from django.contrib.auth.models import User
from .views import *

# Create your tests here.
class TestLoginLogut(TestCase):
    def test_html_hello_user_used(self):
            response = self.client.get('/')
            self.assertTemplateUsed(response, 'landingpage.html')

    def test_hello_user_view_used(self):
        response = resolve('/')
        self.assertEqual(response.func, index)

    def test_login_function_used(self):
        response = resolve('/login/')
        self.assertEqual(response.func, login)

    def test_login_func(self):
        response = self.client.get('/login/')
        self.assertEqual(200, response.status_code)

    def test_login_func_if_form_not_valid(self):
        response = self.client.post('/login/',data={
            'username':'',
            'password':''
        })
        self.assertEqual(200, response.status_code)

    def test_login(self):
        user = User.objects.create_user(username='Reynard', password='ppwkeren')
        response = self.client.post('/login/',data={
            'username' : 'Reynard',
            'password' : 'ppwkeren',
        })
        self.assertEqual(302,response.status_code)

    def test_pake_view_logout(self):
        response = resolve('/logout/')
        self.assertEqual(response.func, logout)